pipeline {
    agent any

    options {
        timestamps ()
        timeout (time: 10, unit: "MINUTES")
        gitLabConnection ("gitlab")
    }

    tools {
        maven "maven"
        jdk "openjdk-8"
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

    environment {
        REPO_URL = "git@gitlab.com:Terre8055/testing.git"
        ANALYTICS_SNAPSHOT = 'analytics-99-SNAPSHOT.jar'
        TELEMETRY_SNAPSHOT = 'telemetry-99-SNAPSHOT.jar'
        SIMULATOR_SNAPSHOT = 'simulator-99-SNAPSHOT.jar'
    }

    stages {

        stage ("Checkout") {
            steps {
                checkout scm
            }
        }

        stage ("Set Version") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    sshagent(['image_pipe']){
                        sh"""
                            git fetch $REPO_URL --tags
                        """
                    }
                    env.VERSION=sh( returnStdout: true, script: "bash getTags.sh").trim()
                    println(env.VERSION)

                    sh"""
                        mvn versions:set -DnewVersion=$env.VERSION
                    """
                }
            }
        }


        stage ("Build -> Test") {
            when {
                branch "feature/*"
            }
            steps {
                script {
                    if ("${env.GIT_COMMIT_MESSAGE}".contains("#e2e")) {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package -Panalytics-e2e
                            """
                        }
                    } else {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package
                            """
                        }
                    }
                }
            }
        }

        stage ("Verify") {
            when {
                branch "main"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn verify
                    """
                    }
                }
            }
        }

        stage("E2E Testing") {
            when {
                branch "main"
            }
            steps {
                script {
                    withCredentials([usernameColonPassword(credentialsId: 'curl_auth', variable: 'curl_auth')]) {
                    
                        def version = "99-SNAPSHOT"
                        def componentJars = ["analytics", "telemetry", "simulator"]

                        def testsFile = version.contains("SNAPSHOT") ? "tests-sanity.txt" : "tests-full.txt"


                        sh "curl -u ${curl_auth} -X GET http://3.109.124.63:8082/artifactory/libs-snapshot-local/com/lidar/simulator/99-SNAPSHOT/simulator-99-SNAPSHOT.jar -o ${SIMULATOR_SNAPSHOT}"
                        sh "curl -u ${curl_auth} -X GET  http://3.109.124.63:8082/artifactory/libs-snapshot-local/com/lidar/telemetry/99-SNAPSHOT/telemetry-99-SNAPSHOT.jar -o ${TELEMETRY_SNAPSHOT}"
                        sh "curl -u ${curl_auth} -X GET http://3.109.124.63:8082/artifactory/libs-snapshot-local/com/lidar/analytics/99-SNAPSHOT/analytics-99-SNAPSHOT.jar -o ${ANALYTICS_SNAPSHOT}"

                        sh "ls -al"

                        sh "java -cp ${SIMULATOR_SNAPSHOT}:${TELEMETRY_SNAPSHOT}:${ANALYTICS_SNAPSHOT}:target/classes com.lidar.simulation.Simulator"
                    }
                }
            }
        }


        stage ("Build -> Test -> Verify -> Publish") {
            when {
                branch "main"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn verify
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Build -> Test -> Verify -> Publish -> Release") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn dependency:list
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Tag") {
            when {
                branch "release/*"
            }
            steps{
                script{
                    sshagent(['image_pipe']){
                        sh"""
                            git clean -f -x
                            git tag $env.VERSION || echo "tag already exists"
                            git push --tags
                        """
                    }

                }
            }
        }

    }

    post {

        always {
            script {
                cleanWs()
            }
        }

        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }

        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
    }
}
